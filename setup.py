from setuptools import setup

setup(name='wonderwoman',
      version='0.1',
      description='Wonder woman python BLE interface',
      url='http://github.com/...',
      author='Nebojsa Stojiljkovic',
      author_email='nebojsa.stojiljkovic@ars-es.com',
      license='BSD',
      packages=['wonderwoman'],
      zip_safe=False)
