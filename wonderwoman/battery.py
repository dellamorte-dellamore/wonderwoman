from .uuid import BATTERY_SERVICE_UUID 
from .uuid import BATTERY_LEVEL_UUID
from .uuid import CCCD_UUID

from bluepy.btle import UUID

class BatterySensor():
    '''
    Battery Service module. Instance the class and enable to get access to Battery interface.
    '''
    serviceUUID = UUID(BATTERY_SERVICE_UUID)  # Ref https://www.bluetooth.com/specifications/gatt/services 
    battery_char_uuid = UUID(BATTERY_LEVEL_UUID) # Ref https://www.bluetooth.com/specifications/gatt/characteristics

    def __init__(self, periph):
        self.periph = periph
        self.battery_service = None
        self.battery_char = None
        self.battery_cccd = None
        self.battery_handle = None
        
    def init(self):
        ''' Enables the class by finding the service and its characteristics. '''
        if self.battery_service is None:
            self.battery_service = self.periph.getServiceByUUID(self.serviceUUID)
        if self.battery_char is None:
            self.battery_char = self.battery_service.getCharacteristics(self.battery_char_uuid)[0]
            self.battery_handle = self.battery_char.getHandle()
            self.battery_cccd = self.battery_char.getDescriptors(forUUID=CCCD_UUID)[0]

    def set_battery_notification(self, state):
        if self.battery_cccd is not None:
            if state == True:
                self.battery_cccd.write(b"\x01\x00", True)
            else:
                self.battery_cccd.write(b"\x00\x00", True)
    
    def disable(self):
        ''' Disable battery service notifications '''
        self.set_battery_notification(False)
        
    def read(self):
        ''' Returns the battery level in percent '''
        val = ord(self.battery_char.read())
        return val
    