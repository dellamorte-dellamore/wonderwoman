from .uuid import DEVICE_INFORMATION_SERVICE_UUID
from .uuid import MANUFACTURER_NAME_STRING_UUID
from .uuid import FIRMWARE_REVISION_STRING_UUID
from .uuid import CCCD_UUID

from bluepy.btle import UUID

class DeviceInformation():
    '''
    Device information module
    '''
    serviceUUID = UUID(DEVICE_INFORMATION_SERVICE_UUID)
    manufacturer_char_uuid = UUID(MANUFACTURER_NAME_STRING_UUID) # Ref https://www.bluetooth.com/specifications/gatt/characteristics
    firmware_revision_char_uuid = UUID(FIRMWARE_REVISION_STRING_UUID) # Ref https://www.bluetooth.com/specifications/gatt/characteristics

    def __init__(self, periph):
        self.periph = periph
        self.device_information_service = None
        self.manufacturer_char = None
        self.manufacturer_cccd = None
        self.manufacturer_handle = None
        self.firmware_revision_char = None
        self.firmware_revision_cccd = None
        self.firmware_revision_handle = None
        
    def init(self):
        ''' Enables the class by finding the service and its characteristics. '''
        if self.device_information_service is None:
            self.device_information_service = self.periph.getServiceByUUID(self.serviceUUID)
        if self.manufacturer_char is None:
            self.manufacturer_char = self.device_information_service.getCharacteristics(self.manufacturer_char_uuid)[0]
            self.manufacturer_handle = self.manufacturer_char.getHandle()
        if self.firmware_revision_char is None:
            self.firmware_revision_char = self.device_information_service.getCharacteristics(self.firmware_revision_char_uuid)[0]
            self.firmware_revision_handle = self.firmware_revision_char.getHandle()

    def read_firmware_revision(self):
        ''' Return firmware revision '''
        val = self.firmware_revision_char.read().decode('utf-8').split('.')
        return val

    def read_manufacturer_name(self):
        ''' Return manufacturer name '''
        val = self.manufacturer_char.read().decode('utf-8')
        return val