from bluepy.btle import UUID

def WonderWoman_UUID(val):
    ''' Adds base UUID and inserts value to return WonderWoman UUID '''
    return UUID("16A2%04X-3916-4598-CD44-A3BA43C1242D" % val)

def write_uint16(data, value, index):
    ''' Write 16bit value into data string at index and return new string '''
    data = data.decode('utf-8')  # This line is added to make sure both Python 2 and 3 works
    return '{}{:02x}{:02x}{}'.format(
                data[:index*4], 
                value & 0xFF, value >> 8, 
                data[index*4 + 4:])

def write_uint8(data, value, index):
    ''' Write 8bit value into data string at index and return new string '''
    data = data.decode('utf-8')  # This line is added to make sure both Python 2 and 3 works
    return '{}{:02x}{}'.format(
                data[:index*2], 
                value, 
                data[index*2 + 2:])
