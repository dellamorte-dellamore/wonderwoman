from bluepy.btle import DefaultDelegate

import struct

class WonderWomanDelegate(DefaultDelegate):
    
    def __init__(self,wonderwoman):
        self.wonderwoman = wonderwoman

    def handleNotification(self, hnd, data):
        raise NotImplementedError

    def _extract_battery_data(self,data): 
        '''Extract battery data'''
        battery, = struct.unpack('B',data)
        return battery

    def _extract_motion_detect_data(self,data): 
        '''Extract motion data data'''
        battery, = struct.unpack('B',data)
        return battery

    def _extract_weight_data(self,data): 
        '''Extract weight data'''
        weight, = struct.unpack('H',data)
        return weight

    