from bluepy.btle import Peripheral,ADDR_TYPE_RANDOM

from .battery import BatterySensor
from .wwcs import WWCSService
from .dev_info import DeviceInformation
from .delegate import WonderWomanDelegate
from .dev_types import *

    
class WonderWoman(Peripheral):
    def __init__(self, addr,dev_id,dev_type,delegate):
        Peripheral.__init__(self, addr, addrType=ADDR_TYPE_RANDOM)
        self.battery = BatterySensor(self)
        self.dev_info = DeviceInformation(self)
        self.wwcs = WWCSService(self,dev_type)
        self.dev_id = dev_id
        '''global members'''
        self.dev_type = dev_type
        self.battery_level = None
        self.motion_detected = None
        self.start_time = None
        self.weight = {}
        self.weight_counter = 0
        
        if dev_type == 'Toothbrush' or dev_type == 'Razorblade':
            self.timeout = SMALL_MOTION_DETECT_TIMEOUT
        elif dev_type == 'WashingMachine':
            self.timeout = LARGE_MOTION_DETECT_TIMEOUT
        if delegate is not None:
            self.setDelegate(delegate)

    def deviceInformationServiceInit(self):
        self.dev_info.init()

    def batteryServiceInit(self):
        self.battery.init()
        
    def wwcsServiceInit(self):
        self.wwcs.init()
        
    def setNotificationBattery(self,enable):
        self.battery.set_battery_notification(enable)
        
    def setNotificationMotion(self,enable):
        self.wwcs.set_motion_detect_notification(enable)

    def setNotificationDoor(self,enable):
        self.wwcs.set_door_notification(enable)
        
    def setNotificationWeight(self,enable):
        self.wwcs.set_weight_notification(enable)
        
    def setWonderWomanDelegate(self,delegate):
        self.setDelegate(delegate)

    def getBatteryLevel(self):
        return self.battery.read()
    
    def getMotionDetect(self):
        return self.wwcs.read_motion_detect_char()

    def getFirmwareRevison(self):
        return self.dev_info.read_firmware_revision()