from .static import WonderWoman_UUID

from .uuid import WWCS_SERVICE_UUID
from .uuid import MOTION_DETECT_CHAR_UUID
from .uuid import WEIGHT_DATA_CHAR_UUID
from .uuid import DOOR_CHAR_UUID
from .uuid import CCCD_UUID
from .uuid import DEVICE_SETUP_CHAR_UUID

from .dev_types import *

import struct

class WWCSService():
    '''
    Motion service module. Instance the class and enable to get access to the Motion interface.
    '''
    serviceUUID =                   WonderWoman_UUID(WWCS_SERVICE_UUID)
    motion_detect_char_uuid =       WonderWoman_UUID(MOTION_DETECT_CHAR_UUID)
    device_setup_char_uuid =        WonderWoman_UUID(DEVICE_SETUP_CHAR_UUID)
    weight_char_uuid =              WonderWoman_UUID(WEIGHT_DATA_CHAR_UUID)
    door_char_uuid =                WonderWoman_UUID(DOOR_CHAR_UUID)
    
    def __init__(self, periph, dev_type):
        self.periph                     = periph
        self.dev_type                   = dev_type
        self.wwcs_service               = None
        self.motion_detect_char         = None
        self.moiton_detect_cccd         = None
        self.motion_detect_handle       = None
        self.door_char                  = None
        self.door_cccd                  = None
        self.door_handle                = None
        self.device_setup_char          = None
        self.device_setup_cccd          = None
        self.device_setup_handle        = None
        self.weight_char                = None
        self.weight_cccd                = None
        self.weight_handle              = None
        
    
    def init(self):
        ''' Enables the class by finding the service and its characteristics. '''
        if self.wwcs_service is None:
            self.wwcs_service = self.periph.getServiceByUUID(self.serviceUUID)
        '''Always exists'''
        if self.device_setup_char is None:
            self.device_setup_char = self.wwcs_service.getCharacteristics(self.device_setup_char_uuid)[0]
            self.device_setup_handle = self.device_setup_char.getHandle()
            dev_id = self.read_device_setup()
            if self.dev_type == 'Toothbrush' \
            or self.dev_type == 'Razorblade':
                if dev_id is not SMALL_MOTION_DETECT_DEVICE:
                    self.write_device_setup(SMALL_MOTION_DETECT_DEVICE)
                    raise Exception('Device ID does not match!')
            elif self.dev_type == 'WashingMachine':
                if dev_id is not LARGE_MOTION_DETECT_DEVICE:
                    self.write_device_setup(LARGE_MOTION_DETECT_DEVICE)
                    raise Exception('Device ID does not match!')
            elif self.dev_type == 'SmallScale':
                if dev_id is not SMALL_WEIGHT_DEVICE:
                    self.write_device_setup(SMALL_WEIGHT_DEVICE)
                    raise Exception('Device ID does not match!')
            elif self.dev_type == 'LargeScale':
                if dev_id is not LARGE_WEIGHT_DEVICE:
                    self.write_device_setup(LARGE_WEIGHT_DEVICE)
                    raise Exception('Device ID does not match!')
        '''Only for motion detect devices ''' 
        if self.dev_type == 'Toothbrush' \
        or self.dev_type == 'Razorblade':
            if self.motion_detect_char is None:
                self.motion_detect_char = self.wwcs_service.getCharacteristics(self.motion_detect_char_uuid)[0]
                self.motion_detect_handle = self.motion_detect_char.getHandle()
                self.motion_detect_cccd = self.motion_detect_char.getDescriptors(forUUID=CCCD_UUID)[0]
        elif self.dev_type == 'WashingMachine':
            if self.motion_detect_char is None:
                self.motion_detect_char = self.wwcs_service.getCharacteristics(self.motion_detect_char_uuid)[0]
                self.motion_detect_handle = self.motion_detect_char.getHandle()
                self.motion_detect_cccd = self.motion_detect_char.getDescriptors(forUUID=CCCD_UUID)[0]
            if self.door_char is None:
                self.door_char = self.wwcs_service.getCharacteristics(self.door_char_uuid)[0]
                self.door_handle = self.door_char.getHandle()
                self.door_cccd = self.door_char.getDescriptors(forUUID=CCCD_UUID)[0]
            
        elif self.dev_type == 'SmallScale' \
        or self.dev_type == 'LargeScale': 
            if self.weight_char is None:
                self.weight_char = self.wwcs_service.getCharacteristics(self.weight_char_uuid)[0]
                self.weight_handle = self.weight_char.getHandle()
                self.weight_cccd = self.weight_char.getDescriptors(forUUID=CCCD_UUID)[0]
    
    def set_door_notification(self, state):
        if self.door_cccd is not None:
            if state == True:
                self.door_cccd.write(b"\x01\x00", True)
            else:
                self.door_cccd.write(b"\x00\x00", True)

    def set_motion_detect_notification(self, state):
        if self.motion_detect_cccd is not None:
            if state == True:
                self.motion_detect_cccd.write(b"\x01\x00", True)
            else:
                self.motion_detect_cccd.write(b"\x00\x00", True)

    def set_weight_notification(self, state):
        if self.weight_cccd is not None:
            if state == True:
                self.weight_cccd.write(b"\x01\x00", True)
            else:
                self.weight_cccd.write(b"\x00\x00", True)

    def disable(self):
        self.set_motion_detect_notification(False)
        self.set_door_notification(False)
        self.set_weight_notification(False)
        
    def read_door_char(self):
        ''' Returns the door level in percent '''
        val, = struct.unpack('b', self.door_char.read())
        return val

    def read_motion_detect_char(self):
        ''' Returns the motion state in percent '''
        val, = struct.unpack('b', self.motion_detect_char.read())
        return val

    def read_device_setup(self):
        ''' Read device type '''
        val, = struct.unpack('b', self.device_setup_char.read())
        return val

    def write_device_setup(self,dev_id):
        ''' Write device setup '''
        self.device_setup_char.write(struct.pack('b',dev_id))
