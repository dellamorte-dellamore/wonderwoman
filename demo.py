from wonderwoman import WonderWoman, WonderWomanDelegate
from bluepy.btle import Scanner
from multiprocessing import Queue,Process
import time
import json
import sys
from iothub_client import IoTHubClient, IoTHubClientError, IoTHubTransportProvider, IoTHubClientResult
from iothub_client import IoTHubMessage, IoTHubMessageDispositionResult, IoTHubError, DeviceMethodReturnValue
import ConfigParser as configparser


MESSAGE_COUNT = 0
MESSAGE_SWITCH = True
MESSAGE_TIMEOUT = 10000

RECEIVE_CONTEXT = 0
TWIN_CONTEXT = 0
METHOD_CONTEXT = 0

SEND_CALLBACKS = 0

TIMEOUT = 241000
MINIMUM_POLLING_TIME = 9
 
PROTOCOL = IoTHubTransportProvider.MQTT
 
process_poll  = {} 
used_mac_pool = {}
inhibit_scan = {}
death_row_queue = Queue(255)
message_queue = Queue(255) 
scanner = Scanner()

def receive_message_callback(message, counter):
    pass

def device_twin_callback(update_state, payload, user_context):
    pass

def device_method_callback(method_name, payload, user_context):
    pass

def send_confirmation_callback(message, result, user_context):
    global SEND_CALLBACKS
    SEND_CALLBACKS += 1
    if result == MESSAGE_TIMEOUT:
        print ( "[Azure] Confirmation received for message [{}] with result = {}".format(user_context, result) )
        print ( "[Azure] Unexpected error from IoTHub")
        print('[Azure] Terminating and restarting Azure IoT Hub process...')
        death_row_queue.put(0)

def iothub_client_init():
    '''prepare iothub client'''
    client = IoTHubClient(CONNECTION_STRING, PROTOCOL)
    
    client.set_option("product_info", "HappyPath_RaspberryPi-Python")
    if client.protocol == IoTHubTransportProvider.HTTP:
        client.set_option("timeout", TIMEOUT)
        client.set_option("MinimumPollingTime", MINIMUM_POLLING_TIME)
    '''set the time until a message times out'''
    client.set_option("messageTimeout", MESSAGE_TIMEOUT)
    '''to enable MQTT logging set to 1'''
    if client.protocol == IoTHubTransportProvider.MQTT:
        client.set_option("logtrace", 0)
    client.set_message_callback(receive_message_callback, RECEIVE_CONTEXT)
    if client.protocol == IoTHubTransportProvider.MQTT or client.protocol == IoTHubTransportProvider.MQTT_WS:
        client.set_device_twin_callback(device_twin_callback, TWIN_CONTEXT)
        client.set_device_method_callback(device_method_callback, METHOD_CONTEXT)
        
    return client

'''
This object handles notifications from each device. 
'''
class NotificationDelegate(WonderWomanDelegate):
    def handleNotification(self,hnd,data):
        '''Handle motion_detected change'''
        if hnd is self.wonderwoman.wwcs.motion_detect_handle:
            motion_detected = self._extract_motion_detect_data(data)
            print('[{}] Motion detected change from {} to {}.'.format(self.wonderwoman.dev_id,self.wonderwoman.motion_detected, motion_detected))
            if motion_detected is 1:          
                '''Motion is started'''
                self.wonderwoman.start_time = time.time()
                self.wonderwoman.motion_detected = 1
                '''Send message to cloud for washing machine only'''
                if self.wonderwoman.dev_type == 'WashingMachine':
                    message = {'deviceId'    : self.wonderwoman.dev_id,
                           'deviceType'      : self.wonderwoman.dev_type,
                           'eventType'       : 'MotionStart',
                           'eventArg'        : 0
                           }
                    message_queue.put(message)
            elif motion_detected is 0:
                '''Motion is ended'''
                self.wonderwoman.motion_detected = 0
                '''send motion stop for all motion devices'''
                if self.wonderwoman.dev_type == 'Toothbrush' \
                or self.wonderwoman.dev_type == 'Razorblade' \
                or self.wonderwoman.dev_type == 'WashingMachine':
                    message = {'deviceId'    : self.wonderwoman.dev_id,
                               'deviceType'  : self.wonderwoman.dev_type,
                               'eventType'   : 'MotionStop',
                               'eventArg'    : self.timediff(t1=self.wonderwoman.start_time,t2=time.time()) - self.wonderwoman.timeout
                    }
                    if message['eventArg'] > 20:
                        message_queue.put(message)
                    else:
                        print('[{}] Motion event duration less than 20s. Discarding event'.format(self.wonderwoman.dev_id))
        if hnd is self.wonderwoman.wwcs.door_handle:
            door_detected = self._extract_motion_detect_data(data)
            if door_detected is 1:          
                '''Send message to cloud for washing machine only'''
                if self.wonderwoman.dev_type == 'WashingMachine':
                    message = {'deviceId'    : self.wonderwoman.dev_id,
                           'deviceType'      : self.wonderwoman.dev_type,
                           'eventType'       : 'Door',
                           'eventArg'        : 1
                           }
                    message_queue.put(message)
        elif hnd is self.wonderwoman.wwcs.weight_handle:
            '''append weight measurement'''
            print('[{}] Received weight: {}g'.format(self.wonderwoman.dev_id,self._extract_weight_data(data)))
            self.wonderwoman.weight[self.wonderwoman.weight_counter] = self._extract_weight_data(data)
            self.wonderwoman.weight_counter += 1
            if(self.wonderwoman.weight_counter>7):
                self.wonderwoman.weight_counter = 0
        elif hnd is self.wonderwoman.battery.battery_handle:
            '''send battery handle'''
            message = {'deviceId': self.wonderwoman.dev_id,
                       'deviceType': self.wonderwoman.dev_type,
                       'eventType': 'Battery',
                       'eventArg': self._extract_battery_data(data)
            }
            message_queue.put(message)                               

    @staticmethod
    def timediff(t1,t2):
        return abs(int(round(t2-t1)))

'''
This process handles BLE connection from WonderWoman devices. 
'''
def ConnectionHandlerRunner(pid,mac_address):
        wonderwoman = None
        ''' Connection handler runner. Used for connecting and initializing device'''
        try:
            print('[{}] Connecting to {}...'.format(device_poll[mac_address][0],mac_address))
            '''Inhibit scan during connecting to peripheral'''
            inhibit_scan[pid]=True
            '''define connection and delegate for process'''
            wonderwoman = WonderWoman(mac_address,device_poll[mac_address][0],device_poll[mac_address][1],None) 
            delegate = NotificationDelegate(wonderwoman)
            wonderwoman.setWonderWomanDelegate(delegate)
            wonderwoman.deviceInformationServiceInit()
            firmware_revison = wonderwoman.getFirmwareRevison()
            print('[{}] Connected! Type: {} Firmware rev: {}.{}.{}!'.format(wonderwoman.dev_id, wonderwoman.dev_type, firmware_revison[0],firmware_revison[1],firmware_revison[2]))
            '''initialize battery and motion services'''
            wonderwoman.batteryServiceInit()
            '''set battery notification and read battery level'''
            wonderwoman.setNotificationBattery(True)
            battery_level = wonderwoman.getBatteryLevel()
            message = {'deviceId': wonderwoman.dev_id,
                       'deviceType': wonderwoman.dev_type,
                       'eventType': 'Battery',
                       'eventArg': battery_level
                        }
            message_queue.put(message)                               
            wonderwoman.wwcsServiceInit()
            '''set weight notification for weight measurement devices'''
            if wonderwoman.dev_type == 'SmallScale' \
            or wonderwoman.dev_type == 'LargeScale':
                print('[{}] Enabling weight notification...'.format(wonderwoman.dev_id))
                wonderwoman.setNotificationWeight(True)
            '''set motion notification for motion detect devices'''
            if wonderwoman.dev_type == 'Toothbrush' \
            or wonderwoman.dev_type == 'Razorblade':
                '''Set motion service notification if needed'''
                wonderwoman.setNotificationMotion(True)
                print('[{}] Enabling motion notification...'.format(wonderwoman.dev_id))
                '''for toothbrush and razorblede assume motion in advance'''
                wonderwoman.motion_detected = 1
                wonderwoman.start_time = time.time()                              
            if wonderwoman.dev_type == 'WashingMachine':
                wonderwoman.setNotificationMotion(True)
                wonderwoman.setNotificationDoor(True)
                '''Read current motion notification state '''
                wonderwoman.motion_detected = wonderwoman.getMotionDetect()
                print('[{}] Motion state read: {}.'.format(wonderwoman.dev_id,wonderwoman.motion_detected))
                '''send detected motion for washing machine'''
                if wonderwoman.motion_detected is 1:
                    wonderwoman.start_time = time.time()
                    '''send message to cloud'''
                    message = {'deviceId': wonderwoman.dev_id,
                               'deviceType': wonderwoman.dev_type,
                               'eventType': 'MotionStart',
                               'eventArg': 0
                    }
                    message_queue.put(message) 
            '''Remove scan inhibition'''
            del inhibit_scan[pid]    
            while True:
                '''Wait for notifications'''
                wonderwoman.waitForNotifications(1)
        except KeyboardInterrupt:
            raise KeyboardInterrupt
        except Exception as ex:
            '''In case of an error put pid to death row queue. Main thread will kill those processes'''
            print('[{}] Terminating process [{}]. Reason: {}'.format(device_poll[mac_address][0], pid, ex))
            if wonderwoman is not None:
                if wonderwoman.dev_type == 'Toothbrush' \
                or wonderwoman.dev_type=='Razorblade' \
                or wonderwoman.dev_type=='WashingMachine':
                    if wonderwoman.motion_detected is 1:
                        '''Send motion stop message to cloud'''
                        message = {'deviceId'    : wonderwoman.dev_id,
                                   'deviceType'  : wonderwoman.dev_type,
                                   'eventType'   : 'MotionStop',
                                   'eventArg'    : NotificationDelegate.timediff(t1=wonderwoman.start_time,t2=time.time())
                        }
                        if message['eventArg'] > 20:
                            message_queue.put(message)
                        else:
                            print('[{}] Motion event duration less than 20s. Discarding event'.format(device_poll[mac_address][0]))
                    '''send weight data to cloud'''
                if wonderwoman.dev_type == 'SmallScale' \
                or wonderwoman.dev_type == 'LargeScale':
                    if wonderwoman.weight != {}:
                        message = {'deviceId'    : wonderwoman.dev_id,
                               'deviceType'  : wonderwoman.dev_type,
                               'eventType' : 'Weight',
                               'eventArg' : wonderwoman.weight[max(wonderwoman.weight,key=wonderwoman.weight.get)]
                               }
                        message_queue.put(message)
            death_row_queue.put(pid)
            return
        
def IoThubClientRunner():
    try:
        client = iothub_client_init()
        while True:
            while message_queue.empty() is not True: 
                MSG_TXT = message_queue.get()
                global MESSAGE_COUNT
                msg_text_formatted = json.dumps(MSG_TXT)
                print ( "[Azure] Sending message [{}]. Message JSON string: {}".format(MESSAGE_COUNT,msg_text_formatted))
                message = IoTHubMessage(msg_text_formatted)
                message.message_id = "message_%d" % MESSAGE_COUNT
                message.correlation_id = "correlation_%d" % MESSAGE_COUNT
                client.send_event_async(message, send_confirmation_callback, MESSAGE_COUNT)
                print ( "[Azure] Accepted message [{}] for transmission to IoT Hub.".format(MESSAGE_COUNT))
                MESSAGE_COUNT += 1
            time.sleep(1)
    except KeyboardInterrupt:
        raise KeyboardInterrupt
    except Exception as ex:
        print ( "[Azure] Unexpected error from IoTHub:\n{}".format(ex))
        print('[Azure] Terminating and restarting Azure IoT Hub process...')
        death_row_queue.put(0)
        return
    
def main():
    global CONNECTION_STRING
    global device_poll
    device_poll = {}
    try:
        '''get configuration from config file'''
        config = configparser.RawConfigParser()
        config.read('config.cfg')
        hostname = config.get('Azure', 'HostName')
        deviceid = config.get('Azure','DeviceId')
        sharedaccesskey = config.get('Azure','SharedAccessKey')
        CONNECTION_STRING = 'HostName={};DeviceId={};SharedAccessKey={}'.format(hostname,deviceid,sharedaccesskey)
        print('[System] Connection string: {}'.format(CONNECTION_STRING))
        devices = config.items('Devices')
        for d in devices:
            dev = ''.join(d[1].split())
            dev = dev.split(',')
            device_poll[dev[0]] = [dev[1],dev[2]]
            print('[System] Found configuration for device: {}, ID: {}, MAC address: {}'.format(dev[2],dev[1],dev[0]))
        pid = 0
        cnt = 0 
        '''create Azure IotHub Client Process'''
        process_poll[pid] =  Process(target=IoThubClientRunner)
        process_poll[pid].start()  
        pid = pid + 1
        while True:
            '''scan for new devices or sleep for 5s'''
            if cnt < len(device_poll.keys()):
                if not inhibit_scan:
                    #print("Scanning...")
                    devices = scanner.scan(5)
                else:
                    #print('Scanning inhibited by connection in progress...')
                    devices = None
                    time.sleep(5)
            else:
                #print("Process poll is full, sleping for 5 seconds...")
                devices = None
                time.sleep(5)      
            '''search for dead processes and remove them from poll. 
            Process 0 is IoT Hub client process, should be restarted'''
            while death_row_queue.empty() is not True: 
                key = death_row_queue.get()
                process_poll[key].terminate()
                del process_poll[key]
                del used_mac_pool[key]
                if key is 0:
                    process_poll[key] =  Process(target=IoThubClientRunner)
                    process_poll[key].start()                     
                else:
                    cnt = cnt - 1
                print('[System] Process [{}] terminated'.format(key))
            
            '''Append new devices if any'''    
            if cnt < len(device_poll.keys()) and devices is not None:
                for d in devices:
                    '''create new thread only if new device on the list is found'''
                    if d.addr in device_poll.keys() and d.getValueText(9) is not None and d.addr not in used_mac_pool.values():
                        print("[System] Created process [{}], type: {}, MAC address: {}".format(device_poll[d.addr][0],pid,device_poll[d.addr][1],d.addr))
                        process_poll[pid] = Process(target = ConnectionHandlerRunner,args=(pid,d.addr))
                        process_poll[pid].start()
                        used_mac_pool[pid] = d.addr
                        pid = pid + 1
                        cnt = cnt + 1
    except Exception as ex:
        print('[System] Main thread encountered an error:\n{}'.format(ex))
        print('[System] Process will now kill all existing subprocesses and terminate...')
        for k in process_poll.keys():
            process_poll[k].terminate()
        print('Done!')
        sys.exit()        

if __name__ == "__main__":
    main()
